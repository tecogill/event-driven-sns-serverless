# Serverless Event Driven applications with SNS

This project demonstrates the communication between to microservices through AWS SNS events

## Prerequisites
- Node.js 14+
- AWS CLI
- Serverless Framework

## Set up
```shell
git clone 
cd event-driven-sns-serverless/user-management

yarn install
```

## Invoke the Lambda function locally:
```shell
yarn invoke:local
```