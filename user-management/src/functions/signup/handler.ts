import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { v4 as uuid } from 'uuid';

import schema, { UserInput, User } from './schema';

const saveUserIntoDatabase = (userInput: UserInput): User => {
  return {
    id: uuid(),
    ...userInput,
  }
}

const signupUser: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  const createdUser = saveUserIntoDatabase(event.body);

  // TODO publish user created event

  return formatJSONResponse({
    message: `The user ${createdUser.name} successfully created with the id ${createdUser.id}`,
    event,
  });
}

export const main = middyfy(signupUser);
