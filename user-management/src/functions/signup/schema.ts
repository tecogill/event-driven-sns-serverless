import { FromSchema } from "json-schema-to-ts";

const userInputSchema = {
  type: "object",
  properties: {
    email: { type: 'string' },
    name: { type: 'string' },
    password: { type: 'string' }
  },
  required: ['email', 'name', 'password']
} as const;

export type UserInput = FromSchema<typeof userInputSchema>

export type User = UserInput & {
  id: string;
}

export default userInputSchema;
